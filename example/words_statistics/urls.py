from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^stats/$', views.overall_statistics),
    url(r'^stats/(?P<slug>[a-zA-Z]+)/$', views.author_statistics),
]
