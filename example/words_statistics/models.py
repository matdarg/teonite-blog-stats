from django.db import models
from unidecode import unidecode

class Word(models.Model):
    word = models.CharField(unique=True, max_length=500)

    def __str__(self):
        return self.word


class Author(models.Model):
    name = models.CharField(unique=True, max_length=50)
    slug = models.SlugField(unique=True)

    def __str__(self):
        return f'{self.name}, {self.slug}'

    def save(self, *args, **kwargs):
        # unidecode is used for change polish signs to english letters i.e. ł to l
        self.slug = unidecode(self.name).lower().replace(' ', '')
        return super(Author, self).save(*args, **kwargs)

class WordStatisticsAuthor(models.Model):
    word = models.ForeignKey(Word, on_delete=models.CASCADE)
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    count = models.IntegerField(default=0)

    def __str__(self):
        return f'{self.word} {self.author} {self.count}'


class WordStatistics(models.Model):
    word = models.OneToOneField(Word, on_delete=models.CASCADE)
    count = models.IntegerField(default=0)

    def __str__(self):
        return f'{self.word} {self.count}'