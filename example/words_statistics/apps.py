from django.apps import AppConfig


class WordsStatisticsConfig(AppConfig):
    name = 'words_statistics'
