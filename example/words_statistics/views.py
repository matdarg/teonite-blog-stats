# Create your views here.
from .models import Word, Author, WordStatistics, WordStatisticsAuthor
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.decorators import api_view, renderer_classes


@api_view(['GET'])
@renderer_classes((JSONRenderer,))
def overall_statistics(request):
    statistics = WordStatistics.objects.all().order_by('-count')[:10]
    content = {statistic.word.word: statistic.count for statistic in statistics}
    return Response(content)


@api_view(['GET'])
@renderer_classes((JSONRenderer,))
def author_statistics(request, slug):
    statistics = WordStatisticsAuthor.objects.filter(author__slug=slug).order_by('-count')[:10]
    content = {statistic.word.word: statistic.count for statistic in statistics}
    return Response(content)