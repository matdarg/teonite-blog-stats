import os
import django
# start django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "example.settings")
django.setup()

from datetime import datetime
import asyncio
import requests
from bs4 import BeautifulSoup
from collections import Counter
import string
from words_statistics import models
from django.db.models import F

request_base = 'https://teonite.com'
blog_uri = request_base + '/blog'
blog_page_uri = blog_uri + '/page'
loop = asyncio.get_event_loop()


def get_number_of_pages_from_response(response):
    """
        get number of page from first blog page
    """
    soup = BeautifulSoup(response.text, 'html.parser')
    page_number = soup.find('span', attrs={'class': 'page-number'}).text
    numbers_in_string = [int(s) for s in page_number.split() if s.isdigit()]
    # return last number from string in example: string: Page 1 of 5 -> returns 5
    return numbers_in_string[-1]


def prepare_links_elements_from_response(response):
    soup = BeautifulSoup(response.text, 'html.parser')
    articles_links.extend(soup.find_all('a', attrs={'class': 'read-more'}))


articles_links = []


async def prepare_articles_links():
    """
    asynchronous preparation list of links to articles
    function send asynchronous requests to all blog pages, gets links to all blog articles,
    and saved them to list_articles
    """
    response = requests.get(blog_uri)
    # process 1st blog page
    number_of_pages = get_number_of_pages_from_response(response)
    prepare_links_elements_from_response(response)

    pages = [
        loop.run_in_executor(
            None,
            requests.get,
            blog_page_uri + f'/{i}/'
        )
        for i in range(2, number_of_pages+1)
    ]
    for response in await asyncio.gather(*pages):
        prepare_links_elements_from_response(response)

word_counts = []

"""cache authors"""
_authors = {}


def cache_get_author_obj(name):
    """database author objects cache"""
    try:
        author_obj = _authors[name]
    except KeyError:
        author_obj, created = models.Author.objects.get_or_create(name=name)
        _authors[name] = author_obj

    return author_obj


"""cache words"""
_words = {}


def cache_get_word_obj(name):
    """database word objects cache"""
    try:
        words_obj = _words[name]
    except KeyError:
        words_obj, created = models.Word.objects.get_or_create(word=name)
        _words[name] = words_obj
    return words_obj


async def prepare_words_from_articles():
    """
    async requests to all articles and prepare word_counts list from articles
    """
    articles_requests = [
        loop.run_in_executor(
            None,
            requests.get,
            request_base + link['href']
        )
        for link in articles_links
    ]

    for response in await asyncio.gather(*articles_requests):
        prepare_word_counts_from_response(response)


def prepare_word_counts_from_response(response):
    """
    extend word_counts list with tuples of author, word, count from articles
    which responds to count of words in auhtor's article
    """
    soup = BeautifulSoup(response.text, 'html.parser')

    # get article author
    author = get_author_name_from_soup(soup)
    # get article content
    text = get_article_text_from_soup(soup)

    # process article content
    text_words_with_punctuation = text.split()
    text_words = [item.strip(string.punctuation) for item in text_words_with_punctuation]
    # create counter object with counts of words
    counter = Counter(text_words)
    # process counter (add to database)
    author_obj = cache_get_author_obj(name=author)
    word_counts.extend((author_obj, word, count) for word, count in counter.items())


def get_article_text_from_soup(soup):
    article = soup.find('section', attrs={'class': 'post-content'})
    # process article content
    return article.text.strip().lower()


def get_author_name_from_soup(soup):
    return soup.find('span', attrs={'class': 'author-content'}).h4.text


async def process_words():
    """prepare async word statistics creation from word_counts list"""
    words = [
        loop.run_in_executor(
            None,
            process_word,
            author_obj, word, count
        )
        for author_obj, word, count in word_counts
    ]
    await asyncio.gather(*words)


def process_word(author_obj, word, count):
    """word statistics database objects creation"""
    word_obj = cache_get_word_obj(name=word)
    process_word_stats(word_obj, count)
    process_word_stats_author(word_obj, author_obj, count)


def process_word_stats(word_obj, count):
    """create word stats database object"""
    word_stats_obj, created = models.WordStatistics.objects.get_or_create(word=word_obj)
    word_stats_obj.count = F('count') + count
    word_stats_obj.save()


def process_word_stats_author(word_obj, author_obj, count):
    """create author word stats database object"""
    word_stats_author_obj, created = models.WordStatisticsAuthor.objects.get_or_create(word=word_obj, author=author_obj)
    word_stats_author_obj.count = F('count') + count
    word_stats_author_obj.save()


if __name__ == '__main__':
    print('init data from articles')
    start = datetime.now()

    # first async loop for prepare all articles links
    loop.run_until_complete(prepare_articles_links())

    # second async loop for prepare all words from articles to process
    loop.run_until_complete(prepare_words_from_articles())

    # async loop for process words and save statistics to database
    loop.run_until_complete(process_words())

    stop = datetime.now()
    print("init data time")
    print(stop-start)

    print("init data loaded")
