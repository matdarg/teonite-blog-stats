#!/bin/sh
echo "waiting for connection to database";
python /code/wait_for_db.py;
echo "Apply database migrations";
python /code/example/manage.py migrate --noinput;
echo "flush all elements from database";
python /code/example/manage.py flush --noinput;
echo "intialize new data to database (if requests exception raised, run script again with docker-compose up)";
python /code/example/initial_script.py;
echo "Starting server";
python /code/example/manage.py runserver 0.0.0.0:8080;